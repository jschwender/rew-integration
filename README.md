# rew-integration

Integrates REW (roomeqwizard) into a debian based linux desktop.

1. removes the install4j mime file.
2. adds an application link (.desktop file)
3. adds a mime file that includes magic detection
4. adds a scalable vector icon for the application and the file's generic icon
5. updates desktop icon- database- and mime-caches

Requires that REW is installed in /opt by root, 
does not work for a user local installation.

Re-install it after each upgrade, by running »apt reinstall rew-integration« if
you have installed it via repository.
